#lang racket
;Spring 2023

; Rotate Elements to the Left Once
(define (rotate-left-1 x)
  (cond
    ((null? x) '()) ; Base case: End of list
    (else (append (cdr x) ; Append car to end of list
                  (cons (car x) '())
))))

(print "Rotate Left Once")(newline)
(rotate-left-1 '())      ; '()
(rotate-left-1 '(a))     ; '(a)
(rotate-left-1 '(a b c)) ; '(b c a)

; Rotate Elements to the Left n Times
(define (rotate-left-n x n)
    (cond
      ((= n 0) x) ; Base case: n is 0
      (else (rotate-left-n (rotate-left-1 x) (- n 1)) ; Recursively cycle list       
)))

(print "Rotate Left n Times")(newline)
(rotate-left-n '(a b c) 0)     ; '(a b c)
(rotate-left-n '(a b c d e) 2) ; '(c d e a b)
(rotate-left-n '(a b c d e) 5) ; '(a b c d e)

; Count Items in List
(define (count-items x)
  (cond
    ((null? x) 0) ; Base case: End of list
    (else (+ (count-items (cdr x)) 1)) ; Count through every element in list
))

(print "Count Items in List")(newline)
(count-items '())          ; 0
(count-items '(a))         ; 1
(count-items '(a b c d e)) ; 5

; Find nth Element in List
(define (list-item-n x n)
  (cond
    ((= n 0) (car x)) ; Base case: n is 0
    (else (list-item-n (cdr x) (- n 1)) ; Cycle through list until base case is reached
)))

(print "List nth Element in List")(newline)
(list-item-n '(a b c d e) 0) ; 'a
(list-item-n '(a b c d e) 4) ; 'e
(list-item-n '(a b c d e) 1) ; 'b

; Remove nth Element from List
(define (list-minus-item-n x n)
  (cond
    ((= n 0) (cdr x)) ; Base case: n is 0
    (else (cons (car x) (list-minus-item-n (cdr x) (- n 1)) ; Cycle through list until n is zero, then disregard index
))))

(print "Remove nth Element from List")(newline)
(list-minus-item-n '(a b c d e) 0) ; '(b c d e)
(list-minus-item-n '(a b c d e) 1) ; '(a c d e)
(list-minus-item-n '(a b c d e) 2) ; '(a b d e)
(list-minus-item-n '(a b c d e) 4) ; '(a b c d)

; Rotate Right Once
(define (rotate-right-1 x)
  (cond
    ((null? x) '()) ; Base case: End of list 
    (else (append (cons (list-item-n x (- (count-items x) 1))'()) ; Recursively cycle with other functions to count length of cdr
                  (list-minus-item-n x (- (count-items x) 1))
))))

(print "Rotate Right Once")(newline)
(rotate-right-1 '(a b c d e))     ; '(e a b c d)
(rotate-right-1 '(a))             ; '(a)
(rotate-right-1 '(a b))           ; '(b a)
(rotate-right-1 '(a b c d e f g)) ; '(g a b c d e f)

; Reverse List
(define (reverse-list x)
  (cond
    ((null? x) '()) ; Base case: End of list
    (else (append (reverse-list (cdr x)) ; Rebuild list by appending car to end of list 
                  (cons (car x) '())               
))))

(print "Reverse List Order")(newline)
(reverse-list '(a))         ; '(a)
(reverse-list '(a b))       ; '(b a)
(reverse-list '(a b c d e)) ; '(e d c b a)

; Construct Car to Every Element
(define (cons-to-all a x)
  (cond
    ((null? x) '()) ; Base case: End of list
    (else (append (cons (cons a (car x)) ; Cons a then append cdr, recursively 
                  (cons-to-all a (cdr x))        
)))))

(print "Construct Car to Every Element")(newline)
(cons-to-all 'a '((b c) (d e) (f g))) ; '((a b c) (a d e) (a f g))

; Permute x
(define (permute x)
  (cond
    ((empty? x) null) ; Base case 1: End of list
    ((empty? (cdr x))(list x)) ; Base case 2: 1 element 
    ((empty? (cddr x))(list x (reverse x))) ; Base case 3: 2 elements, reverse second one
    (else (ph-2 x(-(length x) 1)) ; List greater than 2, request helpers to build list 
)))

(define (ph-1 x n)
  (cons-to-all (list-item-n x n) ; Build smaller combinations with permute's base cases
               (permute (list-minus-item-n x n))
))
(define (ph-2 x n)
  (cond
    ((= n 0)(ph-1 x n)) ; Base case: Send to helper 1 to finish
    (else (append (ph-1 x n)(ph-2 x (- n 1)) ; Use helper 1 to build list
))))

(print "Generate All Permutations in List")(newline)
(permute '(a b))
; '((a b) (b a))
(permute '(a b c))
; '((c a b) (c b a) (b a c) (b c a) (a b c) (a c b)) 
(permute '(a b c d))
; '((d c a b) (d c b a) (d b a c) (d b c a) (d a b c)
; (d a c b) (c d a b) (c d b a) (c b a d) (c b d a)
; (c a b d) (c a d b) (b d a c) (b d c a) (b c a d)
; (b c d a) (b a c d) (b a d c) (a d b c) (a d c b)
; (a c b d) (a c d b) (a b c d) (a b d c))

;Return last value in a list

(define (list-last x)
  (cond
    ((empty? x) null) ; if empty, end
    ((empty? (cdr x)) x) ; if one or last element, end
    (else (list-last (cdr x))) ; recursively call list
))

(displayln "Test Case #1")
(list-last '())      ; '()
(list-last '(a))     ; '(a)
(list-last '(a b c)) ; '(c)

;Check if all items in a list are numbers

(define (list-numbers? x)
  (cond
    ((empty? x) #false) ; if empty, return false
    ((empty? (cdr x)) ; if single element, check:
     (cond
       ((number? (car x)) #true) ; if element is number
       (else #false))) ; if element is not number
    ((number? (car x)) (list-numbers? (cdr x))) ; if element is number, move on
    ((not(number? (car x))) list-numbers? #false) ; if element is not number, end
    (else (list-numbers? (cdr x))) ; recursively call list
))

(displayln "Test Case #2")
(list-numbers? '()) ; #f
(list-numbers? '(1)) ; #t
(list-numbers? '(a)) ; #f
(list-numbers? '(1 2 3 4 5)) ; #t
(list-numbers? '(a 2 3 4 5)) ; #f
(list-numbers? '(1 2 a 4 5)) ; #f
(list-numbers? '(1 2 3 4 a)) ; #f

;Check if items in a list are in ascending order

(define (sorted? x)
  (cond
    ((empty? x) #true) ; if list empty, return true
    ((empty? (cdr x)) #true) ; if list one element, return true
    ((< (car x) (cadr x)) (sorted? (cdr x))) ; if ascending, move on
    ((> (car x) (cadr x)) #false) ; if descending, return false and end
    (else (sorted? (cdr x))) ; recursively call list
))

(displayln "Test Case #3")
(sorted? '()) ; #t
(sorted? '(1)) ; #t
(sorted? '(1 2 3 4 5)) ; #t
(sorted? '(1 1 1 1 1)) ; #t
(sorted? '(2 1 2 3 4)) ; #f
(sorted? '(1 2)) ; #t
(sorted? '(2 1)) ; #f

;Adjust factorial function with list-gen 

(define (list-gen n)
  (cond
    ((= n 1)(list 1)) ; if one element, return
    (else (append (list-gen (- n 1))(list n))) ; recursively call list
))

(define (f n)
  (apply * (list-gen n)) ; increase call, factorially(sp?)
)

(displayln "Test Case #4")
(list-gen 5) ; '(1 2 3 4 5)
(f 1) ; 1
(f 2) ; 2
(f 3) ; 6
(f 4) ; 24
(f 5) ; 120
(f 6) ; 720
